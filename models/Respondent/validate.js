const Validator = require('validator')
const isEmpty = require('../../helpers/is-empty')

module.exports = function validateRespondentInput(data) {
  let errors = {}

  // name
  data.name = !isEmpty(data.name) ? data.name : ''
  if (!Validator.isLength(data.name, { min: 5, max: 500 })) {
    errors.name = 'Имя от 5 до 500 символов'
  }
  if (Validator.isEmpty(data.name)) {
    errors.name = 'Имя обязательно'
  }

  // code
  data.code = !isEmpty(data.code) ? data.code : ''
  if (Validator.isEmpty(data.code)) {
    errors.code = 'Код обязателен'
  }

  // sid
  data.sid = !isEmpty(data.sid) ? data.sid : ''
  if (Validator.isEmpty(data.sid)) {
    errors.sid = 'привязка к исследованию обязательна'
  }

  // link
  data.link = !isEmpty(data.link) ? data.link : ''

  return {
    errors,
    isValid: isEmpty(errors)
  }
}
