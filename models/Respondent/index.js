const mongoose = require('mongoose')
const Schema = mongoose.Schema

const RespondentSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  code: {
    type: String,
    required: true
  },
  sid: {
    type: String,
    required: true
  },
  link: {
    type: String
  }
})

module.exports = Respondent = mongoose.model('respondent', RespondentSchema)
