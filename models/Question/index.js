const mongoose = require('mongoose')
const Schema = mongoose.Schema

const QuestionShema = new Schema({
  title: {
    type: String,
    required: true
  },
  type: {
    type: String,
    required: true
  },
  sid: {
    type: String,
    required: true
  },
  criterias: [
    {
      criteriaId: {
        type: Schema.Types.ObjectId,
        ref: 'criteria'
      },
      title: {
        type: String
      }
    }
  ]
})

module.exports = Question = mongoose.model('question', QuestionShema)
