const Validator = require('validator')
const isEmpty = require('../../helpers/is-empty')

module.exports = function validateSurveyInput(data) {
  let errors = {}

  // title
  data.title = !isEmpty(data.title) ? data.title : ''
  if (!Validator.isLength(data.title, { min: 5, max: 500 })) {
    errors.title = 'Название от 5 до 500 символов'
  }
  if (Validator.isEmpty(data.title)) {
    errors.title = 'Название обязательно'
  }

  // type
  data.type = !isEmpty(data.type) ? data.type : ''
  if (Validator.isEmpty(data.type)) {
    errors.type = 'Тип вопроса обязателен'
  }

  // criterias
  data.criterias = !isEmpty(data.criterias) ? data.criterias : []

  // sid
  data.sid = !isEmpty(data.sid) ? data.sid : ''
  if (Validator.isEmpty(data.sid)) {
    errors.sid = 'Привязка к опросу обязательна'
  }

  return {
    errors,
    isValid: isEmpty(errors)
  }
}
