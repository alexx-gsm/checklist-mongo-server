const mongoose = require('mongoose')
const Schema = mongoose.Schema

const SurveyShema = new Schema({
  title: {
    type: String,
    required: true
  },
  interval: {
    start: {
      type: Date,
      default: Date.now
    },
    finish: {
      type: Date
    }
  },
  comment: {
    type: String
  },
  rid: {
    type: String,
    required: true
  },
  criteriaGroups: {
    type: Object
  },
  respondents: [
    {
      respondentId: {
        type: Schema.Types.ObjectId,
        ref: 'respondent'
      }
    }
  ],
  isRun: {
    type: Boolean,
    default: false
  }
})

module.exports = Survey = mongoose.model('survey', SurveyShema)
