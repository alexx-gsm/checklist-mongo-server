const Validator = require('validator')
const isEmpty = require('../../helpers/is-empty')

module.exports = function validateSurveyInput(data) {
  let errors = {}

  // title
  data.title = !isEmpty(data.title) ? data.title : ''
  if (!Validator.isLength(data.title, { min: 5, max: 50 })) {
    errors.title = 'Название от 5 до 50 символов'
  }
  if (Validator.isEmpty(data.title)) {
    errors.title = 'Название обязательно'
  }

  // comment
  data.comment = !isEmpty(data.comment) ? data.comment : ''

  // criteriaGroups
  data.criteriaGroups = !isEmpty(data.criteriaGroups) ? data.criteriaGroups : []

  // respondents
  data.respondents = !isEmpty(data.respondents) ? data.respondents : []

  // isRun
  data.isRun = !isEmpty(data.isRun) ? data.isRun : false

  return {
    errors,
    isValid: isEmpty(errors)
  }
}
