const mongoose = require('mongoose')
const Schema = mongoose.Schema

const ResultShema = new Schema({
  surveyId: {
    type: Schema.Types.ObjectId,
    ref: 'survey'
  },
  respondentId: {
    type: Schema.Types.ObjectId,
    ref: 'respondent'
  },
  isDone: {
    type: Boolean,
    default: false
  },
  link: {
    type: String,
    required: true
  },
  summary: {
    type: Object
  }
})

module.exports = Result = mongoose.model('result', ResultShema)
