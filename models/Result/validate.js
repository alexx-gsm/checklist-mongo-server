const Validator = require('validator')
const isEmpty = require('../../helpers/is-empty')

module.exports = function validateResultInput(data) {
  let errors = {}

  // surveyId
  data.surveyId = !isEmpty(data.surveyId) ? data.surveyId : ''
  if (Validator.isEmpty(data.surveyId)) {
    errors.surveyId = 'привязка к опросу обязательна'
  }

  // respondentId
  data.respondentId = !isEmpty(data.respondentId) ? data.respondentId : ''
  if (Validator.isEmpty(data.respondentId)) {
    errors.respondentId = 'привязка к респонденту обязательна'
  }

  // isDone
  data.isDone = !isEmpty(data.isDone) ? data.isDone : false

  // link
  data.link = !isEmpty(data.link) ? data.link : ''

  // summary
  data.summary = !isEmpty(data.summary) ? data.summary : {}

  return {
    errors,
    isValid: isEmpty(errors)
  }
}
