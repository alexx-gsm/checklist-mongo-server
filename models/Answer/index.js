const mongoose = require('mongoose')
const Schema = mongoose.Schema

const AnswerShema = new Schema({
  title: {
    type: String,
    required: true
  },
  type: {
    type: String,
    required: true
  },
  variants: [
    {
      title: {
        type: String,
        default: ''
      },
      weight: {
        type: String,
        default: '0'
      }
    }
  ]
})

module.exports = Answer = mongoose.model('answer', AnswerShema)
