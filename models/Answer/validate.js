const Validator = require('validator')
const isEmpty = require('../../helpers/is-empty')

module.exports = function validateAnswerInput(data) {
  let errors = {}

  // title
  data.title = !isEmpty(data.title) ? data.title : ''
  if (!Validator.isLength(data.title, { min: 5, max: 500 })) {
    errors.title = 'Название от 5 до 500 символов'
  }
  if (Validator.isEmpty(data.title)) {
    errors.title = 'Название обязательно'
  }

  // type
  data.type = !isEmpty(data.type) ? data.type : ''
  if (!Validator.isLength(data.type, { min: 5, max: 500 })) {
    errors.type = 'Название от 5 до 500 символов'
  }
  if (Validator.isEmpty(data.type)) {
    errors.type = 'Название обязательно'
  }

  // variants
  data.variants = !isEmpty(data.variants) ? data.variants : []

  return {
    errors,
    isValid: isEmpty(errors)
  }
}
