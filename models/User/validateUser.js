const Validator = require('validator')
const isEmpty = require('../../helpers/is-empty')

module.exports = function validateUserInput(data) {
  let errors = {}

  // name
  data.name = !isEmpty(data.name) ? data.name : ''
  if (!Validator.isLength(data.name, { min: 5, max: 50 })) {
    errors.name = 'ФИО от 5 до 50 символов'
  }
  // if (!Validator.isAlpha(data.name.replace(/ /g, ''), 'ru-RU')) {
  //   errors.name = 'ФИО содержит только киррилицу'
  // }
  if (Validator.isEmpty(data.name)) {
    errors.name = 'ФИО обязательно'
  }

  //   // nameEn
  //   data.nameEn = !isEmpty(data.nameEn) ? data.nameEn : ''
  //   if (!Validator.isLength(data.nameEn, { min: 3, max: 20 })) {
  //     errors.nameEn = 'NickName от 3 до 20 символов'
  //   }
  //   if (!Validator.isAlpha(data.nameEn.replace(/ /g, ''), 'en-US')) {
  //     errors.nameEn = 'NickName содержит только латиницу'
  //   }
  //   if (Validator.isEmpty(data.nameEn)) {
  //     errors.nameEn = 'NickName обязательно'
  //   }

  // email
  data.email = !isEmpty(data.email) ? data.email : ''
  if (Validator.isEmpty(data.email)) {
    errors.email = 'Email field is required'
  }
  if (!Validator.isEmail(data.email)) {
    errors.email = 'Email is invalid'
  }

  // // password
  // data.password = !isEmpty(data.password) ? data.password : ''
  // if (!Validator.isLength(data.password, { min: 6, max: 50 })) {
  //   errors.password = 'Password field must be between 6 and 50 characters'
  // }
  // if (Validator.isEmpty(data.password)) {
  //   errors.password = 'Password field is required'
  // }

  // // password2
  // data.password2 = !isEmpty(data.password2) ? data.password2 : ''
  // if (Validator.isEmpty(data.password2)) {
  //   errors.password2 = 'Confirm Password field is required'
  // }
  // if (!Validator.equals(data.password, data.password2)) {
  //   errors.password2 = 'Passwords must match'
  // }

  // role
  data.role = !isEmpty(data.role) ? data.role : ''
  if (Validator.isEmpty(data.role)) {
    errors.role = 'Role field is required'
  }

  return {
    errors,
    isValid: isEmpty(errors)
  }
}
