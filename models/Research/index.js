const mongoose = require('mongoose')
const Schema = mongoose.Schema

const ResearchShema = new Schema({
  title: {
    type: String,
    required: true
  },
  interval: {
    start: {
      type: Date,
      default: Date.now
    },
    finish: {
      type: Date
    }
  },
  comment: {
    type: String
  },
  root: {
    type: String,
    required: true
  }
})

module.exports = Research = mongoose.model('research', ResearchShema)
