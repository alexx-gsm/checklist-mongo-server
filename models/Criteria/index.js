const mongoose = require('mongoose')
const Schema = mongoose.Schema

const CriteriaShema = new Schema({
  title: {
    type: String,
    required: true
  },
  sid: {
    type: String,
    required: true
  }
})

module.exports = Criteria = mongoose.model('criteria', CriteriaShema)
