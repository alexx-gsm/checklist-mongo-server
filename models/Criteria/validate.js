const Validator = require('validator')
const isEmpty = require('../../helpers/is-empty')

module.exports = function validateCriteriaInput(data) {
  let errors = {}

  // title
  data.title = !isEmpty(data.title) ? data.title : ''
  if (!Validator.isLength(data.title, { min: 5, max: 500 })) {
    errors.title = 'Название от 5 до 500 символов'
  }
  if (Validator.isEmpty(data.title)) {
    errors.title = 'Название обязательно'
  }

  // sid
  data.sid = !isEmpty(data.sid) ? data.sid : ''
  if (Validator.isEmpty(data.sid)) {
    errors.sid = 'привязка к опросу обязательна'
  }

  return {
    errors,
    isValid: isEmpty(errors)
  }
}
