const express = require('express')
const router = express.Router()
const gravatar = require('gravatar')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const keys = require('../../config/keys')
const passport = require('passport')

// Load Input Validation
const validateRegisterInput = require('../../models/User/validateRegister')
const validateLoginInput = require('../../models/User/validateLogin')
const validateUserInput = require('../../models/User/validateUser')

/**
 * ! @route   POST api/users/register
 * ? @desc    Register user
 * * @access  Public
 */
router.post('/register', (req, res) => {
  const { errors, isValid } = validateRegisterInput(req.body)

  // Check Validation
  if (!isValid) {
    return res.status(400).json(errors)
  }

  User.findOne({ email: req.body.email }).then(user => {
    if (user) {
      errors.email = 'Email already exists'

      return res.status(400).json(errors)
    } else {
      const avatar = gravatar.url(req.body.email, {
        s: '200', // Size
        r: 'pg', // Rating
        d: 'mm' // Default
      })

      const newUser = new User({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password,
        role: req.body.role,
        root: req.body.root,
        avatar
      })

      bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(newUser.password, salt, (err, hash) => {
          if (err) throw err
          newUser.password = hash
          newUser
            .save()
            .then(user => res.json(user))
            .catch(err => console.log(err))
        })
      })
    }
  })
})

/**
 * ! @route   GET api/users/login
 * ? @desc    Login User / Returning JWT Token
 * * @access  Public
 */
router.post('/login', (req, res) => {
  const { errors, isValid } = validateLoginInput(req.body)
  console.log('login')

  // Check Validation
  if (!isValid) {
    return res.status(400).json(errors)
  }

  const email = req.body.email
  const password = req.body.password

  // Find User by email
  User.findOne({ email }).then(user => {
    // Check fo User
    if (!user) {
      errors.email = 'User not found'
      return res.status(404).json(errors)
    }

    // Check password
    bcrypt.compare(password, user.password).then(isMatch => {
      if (isMatch) {
        // User matched

        const payload = {
          // create JWT payload
          id: user.id,
          name: user.name,
          avatar: user.avatar,
          role: user.role,
          root: user.root
        }

        // Sign Token
        jwt.sign(
          payload,
          keys.secretOrKey,
          { expiresIn: 3600000 },
          (err, token) => {
            res.json({
              success: true,
              token: 'Bearer ' + token
            })
          }
        ) // 1 hour
      } else {
        errors.password = 'Password incorrect'
        return res.status(400).json(errors)
      }
    })
  })
})

/**
 * ! @route   POST api/users
 * ? @desc    Update user
 * ! @access  Private
 */
router.put(
  '/',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    // TODO: check 'admin' role

    // validate user input
    const { errors, isValid } = validateUserInput(req.body)
    if (!isValid) {
      return res.status(400).json(errors)
    }

    const { _id, email, name, role, root } = req.body
    if (_id) {
      const userFields = {
        name,
        email,
        role,
        root
      }
      try {
        User.findById(_id).then(user => {
          if (user) {
            User.findOneAndUpdate(
              { _id },
              { $set: userFields },
              { new: true }
            ).then(user => res.json(user))
          }
        })
      } catch (error) {
        res.status(400).json({ error })
      }
    }
    // res.status(401).json({ nouser: 'not found' })
  }
)

/**
 * ! @route   POST api/users/all
 * * @desc    Get all users
 * ! @access  Private
 */
router.post(
  '/all',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    // TODO: check 'admin' role

    try {
      User.find().then(users => res.json(users))
    } catch (error) {
      res.status(400).json({ error })
    }
  }
)

/**
 * ! @route   POST api/users/:id
 * * @desc    Get user by id
 * ! @access  Private
 */
router.post(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    // TODO: check 'admin' role

    try {
      User.findById(req.params.id).then(user => res.json(user))
    } catch (error) {
      res.status(400).json({ error })
    }
  }
)

// @route   GET api/users/current
// @desc    Return current user
// @access  Private
router.get(
  '/current',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    res.json(req.user)
  }
)

module.exports = router
