var express = require('express')
var router = express.Router()
const passport = require('passport')

// Load Models
const Survey = require('../../models/Survey')
// Load validation
const validateSurveyInput = require('../../models/Survey/validate')

/**
 * ! @route   POST api/survey
 * * @desc    Save research
 * ! @access  Private
 */
router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    // validate
    const { errors, isValid } = validateSurveyInput(req.body)
    if (!isValid) {
      return res.status(400).json(errors)
    }

    const { _id } = req.body

    const fields = {
      title: req.body.title,
      comment: req.body.comment,
      rid: req.body.rid,
      interval: req.body.interval,
      criteriaGroups: req.body.criteriaGroups,
      respondents: req.body.respondents,
      isRun: req.body.isRun
    }

    try {
      if (_id) {
        Survey.findOne({ _id }).then(survey => {
          if (survey) {
            Survey.findOneAndUpdate(
              { _id },
              { $set: fields },
              { new: true }
            ).then(survey => res.json(survey))
          }
        })
      } else {
        const newSurvey = new Survey(fields)
        newSurvey.save().then(survey => res.json(survey))
      }
    } catch (err) {
      res.status(400).json(err)
    }
  }
)

/**
 * ! @route   POST api/survey/all
 * * @desc    Get All Surveys for this research (rid)
 * @param     rid - research Id
 * ! @access  Private
 */
router.post(
  '/all',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    // TODO: check user access rights for research && surveys

    const { rid } = req.body
    try {
      if (rid) {
        res.json(await Survey.find({ rid }))
      } else {
        res.json(await Survey.find())
      }
    } catch (err) {
      res.status(400).json(err)
    }
  }
)

/**
 * ! @route   POST api/survey/:id
 * * @desc    Get Survey by id
 * @param     id - survey Id
 * ! @access  Private
 */
router.post(
  '/:_id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    // TODO: check user access rights for survey

    const { _id } = req.params
    try {
      if (_id) {
        Survey.findOne({ _id }).then(survey => res.json(survey))
      }
    } catch (err) {
      res.status(400).json(err)
    }
  }
)

module.exports = router
