var express = require('express')
var router = express.Router()
const passport = require('passport')

// Load Models
const Research = require('../../models/Research')
// Load validation
const validateResearchInput = require('../../models/Research/validate')

/**
 * ! @route   POST api/researches
 * * @desc    Save research
 * ! @access  Private
 */
router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    // validate user input
    const { errors, isValid } = validateResearchInput(req.body)
    if (!isValid) {
      return res.status(400).json(errors)
    }

    const { _id, title, comment, root, interval } = req.body
    try {
      if (_id) {
        const researchFields = {
          title,
          comment,
          root,
          interval
        }
        Research.findOne({ _id }).then(research => {
          if (research) {
            Research.findOneAndUpdate(
              { _id },
              { $set: researchFields },
              { new: true }
            ).then(research => res.json(research))
          }
        })
      } else {
        const newResearch = new Research({
          title,
          comment,
          root,
          interval
        })
        newResearch.save().then(research => res.json(research))
      }
    } catch (err) {
      res.status(400).json(err)
    }
  }
)

/**
 * ! @route   POST api/researches/user/all
 * * @desc    Get all researches by user
 * ! @access  Private
 */
router.post(
  '/user/all',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const root = req.user.root === '0' ? req.user._id : req.user.root
    try {
      Research.find({ root }).then(researches => res.json(researches))
    } catch (err) {
      res.status(400).json(err)
    }
  }
)

/**
 * ! @route   POST api/research/:id
 * * @desc    Get research by id
 * ! @access  Private
 */
router.post(
  '/user/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const root = req.user.root === '0' ? req.user._id : req.user.root
    try {
      Research.findOne({ root, _id: req.params.id }).then(research =>
        res.json(research)
      )
    } catch (err) {
      res.status(400).json(err)
    }
  }
)

module.exports = router
