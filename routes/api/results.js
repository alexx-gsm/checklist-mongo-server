var express = require('express')
var router = express.Router()
const passport = require('passport')

// Load Models
const Result = require('../../models/Result')
const Respondent = require('../../models/Respondent')
const Question = require('../../models/Question')
const Criteria = require('../../models/Criteria')
// Load validation
const validateResultInput = require('../../models/Result/validate')

/**
 * ! @route   POST api/results/data/:link
 * * @desc    Get Result by id
 * ? @param   id: result Id
 * ! @access  Private
 */
router.post(
  '/data/:link',
  // passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    // TODO: check user access rights for result

    const { link } = req.params

    try {
      if (link) {
        const result = await Result.findOne({ link })
        const respondents = await Respondent.find({ sid: result.surveyId })
        const questions = await Question.find({ sid: result.surveyId })
        const criterias = await Criteria.find({ sid: result.surveyId })

        res.json({
          questions,
          respondents,
          criterias,
          summary: result.summary
        })
      }
    } catch (err) {
      res.status(400).json(err)
    }
  }
)

/**
 * ! @route   PUT api/results/data/:link
 * * @desc    Save results
 * ? @param   id: result Id
 * ! @access  Private
 */
router.put(
  '/data/:link',
  // passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    // TODO: check user access rights for result

    const { link } = req.params

    const { payload } = req.body

    try {
      if (link) {
        const _result = await Result.findOne({ link })
        const newSummary = {
          ..._result.summary,
          ...payload
        }
        const newResult = await Result.findOneAndUpdate(
          { link },
          { $set: { summary: newSummary } },
          { new: true }
        )
        res.json(newResult.summary)
      }
    } catch (err) {
      res.status(400).json(err)
    }
  }
)

/**
 * ! @route   POST api/results
 * * @desc    Save result
 * ! @access  Private
 */
router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    // validate survey input
    const { errors, isValid } = validateResultInput(req.body)
    if (!isValid) {
      return res.status(400).json(errors)
    }

    const { _id } = req.body

    const itemFields = {
      surveyId: req.body.surveyId,
      respondentId: req.body.respondentId,
      isDone: req.body.isDone,
      link: req.body.link,
      summary: req.body.summary
    }

    try {
      if (_id) {
        res.json(
          await Result.findOneAndUpdate(
            { _id },
            { $set: itemFields },
            { new: true, upsert: true }
          )
        )
      } else {
        const newResult = new Result(itemFields)
        res.json(await newResult.save())
      }
    } catch (err) {
      res.status(400).json(err)
    }
  }
)

/**
 * ! @route   POST api/results/all
 * * @desc    Get All Results
 * ? @param   sid: survey Id
 * ! @access  Private
 */
router.post(
  '/all',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    // TODO: check user access rights

    const { sid } = req.body

    try {
      if (sid) {
        res.json(await Result.find({ sid }))
      }
    } catch (err) {
      res.status(400).json(err)
    }
  }
)

/**
 * ! @route   POST api/results/:id
 * * @desc    Get Result by id
 * ? @param   id: result Id
 * ! @access  Private
 */
router.post(
  '/:_id',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    // TODO: check user access rights for result

    const { _id } = req.params

    try {
      if (_id) {
        res.json(await Result.findOne({ link: _id }))
      }
    } catch (err) {
      res.status(400).json(err)
    }
  }
)

module.exports = router
