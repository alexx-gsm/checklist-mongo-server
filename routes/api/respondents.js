var express = require('express')
var router = express.Router()
const passport = require('passport')

// Load Models
const Respondent = require('../../models/Respondent')
// Load validation
const validateRespondentInput = require('../../models/Respondent/validate')

/**
 * ! @route   POST api/respondents/upload
 * * @desc    Insert multiple respondents
 * ? @param   sid: survey Id
 * ! @access  Private
 */
router.post(
  '/upload',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    // TODO: check user access rights

    const { respondents, sid } = req.body

    try {
      if (sid && respondents) {
        await Respondent.deleteMany({ sid })
        const items = await Respondent.insertMany(respondents)

        res.json(items)
      }
    } catch (err) {
      res.status(400).json(err)
    }
  }
)

/**
 * ! @route   POST api/respondents
 * * @desc    Save respondent
 * ! @access  Private
 */
router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    // validate survey input
    const { errors, isValid } = validateRespondentInput(req.body)
    if (!isValid) {
      return res.status(400).json(errors)
    }

    const { _id } = req.body

    const itemFields = {
      name: req.body.name,
      code: req.body.code,
      sid: req.body.sid,
      link: req.body.link
    }

    try {
      if (_id) {
        res.json(
          await Respondent.findOneAndUpdate(
            { _id },
            { $set: itemFields },
            { new: true, upsert: true }
          )
        )
      } else {
        const newRespondent = new Respondent(itemFields)
        res.json(await newRespondent.save())
      }
    } catch (err) {
      res.status(400).json(err)
    }
  }
)

/**
 * ! @route   POST api/respondents/all
 * * @desc    Get All Respondents
 * ? @param   sid: survey Id
 * ! @access  Private
 */
router.post(
  '/all',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    // TODO: check user access rights

    const { sid } = req.body

    try {
      if (sid) {
        res.json(await Respondent.find({ sid }))
      } else {
        res.json(await Respondent.find())
      }
    } catch (err) {
      res.status(400).json(err)
    }
  }
)

/**
 * ! @route   POST api/respondents/:id
 * * @desc    Get Respondent by id
 * ? @param   id: respondent Id
 * ! @access  Private
 */
router.post(
  '/:_id',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    // TODO: check user access rights for respondent

    const { _id } = req.params

    try {
      if (_id) {
        res.json(await Respondent.findOne({ _id }))
      }
    } catch (err) {
      res.status(400).json(err)
    }
  }
)

module.exports = router
