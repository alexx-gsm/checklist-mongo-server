var express = require('express')
var router = express.Router()
const passport = require('passport')

// Load Models
const Question = require('../../models/Question')
const Criteria = require('../../models/Criteria')
// Load validation
const validateQuestionInput = require('../../models/Question/validate')

/**
 * ! @route   POST api/questions
 * * @desc    Save question
 * ! @access  Private
 */
router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    // validate survey input
    const { errors, isValid } = validateQuestionInput(req.body)
    if (!isValid) {
      return res.status(400).json(errors)
    }

    const { _id } = req.body

    const itemFields = {
      title: req.body.title,
      type: req.body.type,
      sid: req.body.sid,
      criterias: req.body.criterias
    }

    try {
      if (_id) {
        res.json(
          await Question.findOneAndUpdate(
            { _id },
            { $set: itemFields },
            { new: true, upsert: true }
          )
        )
      } else {
        const newQuestion = new Question(itemFields)
        newQuestion.save().then(question => res.json(question))
      }
    } catch (err) {
      console.log('err', err)
      res.status(400).json(err)
    }
  }
)

/**
 * ! @route   POST api/question/all
 * * @desc    Get All Surveys for this research (rid)
 * @param     rid - research Id
 * ! @access  Private
 */
router.post(
  '/all',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    // TODO: check user access rights

    const { sid } = req.body
    try {
      if (sid) {
        Question.find({ sid }).then(questions => res.json(questions))
      } else {
        res.status(404)
      }
    } catch (err) {
      res.status(400).json(err)
    }
  }
)

/**
 * ! @route   POST api/question/:id
 * * @desc    Get Survey by id
 * ? @param   _id: question Id
 * ! @access  Private
 */
router.post(
  '/:_id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    // TODO: check user access rights for question

    const { _id } = req.params
    try {
      if (_id) {
        Question.findOne({ _id }).then(question => res.json(question))
      }
    } catch (err) {
      res.status(400).json(err)
    }
  }
)

module.exports = router
