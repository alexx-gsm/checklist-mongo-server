var express = require('express')
var router = express.Router()
const passport = require('passport')

// Load Models
const Role = require('../../models/Role')

/**
 * ! @route   POST api/roles/all
 * * @desc    Get all roles
 * ! @access  Private
 */
router.post(
  '/all',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    try {
      Role.find().then(roles => res.json(roles))
    } catch (err) {
      res.status(400).json(err)
    }
  }
)

/**
 * ! @route   POST api/roles/:id
 * * @desc    Get role by id
 * ! @access  Private
 */
router.post(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    try {
      roleRef.once(
        'value',
        snapshot => {
          const roles = snapshot.val()
          if (roles) {
            const id = req.params.id
            res.json({ id, ...roles[id] })
          } else {
            errors.role = 'role not found'
            res.status(400).json(errors)
          }
        },
        err => {
          console.log('err:', err.code)
          res.status(400).json(err)
        }
      )
    } catch (err) {
      res.status(400).json(err)
    }
  }
)

module.exports = router
