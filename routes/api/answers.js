var express = require('express')
var router = express.Router()
const passport = require('passport')

// Load Models
const Answer = require('../../models/Answer')
// Load validation
const validateAnswerInput = require('../../models/Answer/validate')

/**
 * ! @route   POST api/answers
 * * @desc    Save answer
 * ! @access  Private
 */
router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    // validate survey input
    const { errors, isValid } = validateAnswerInput(req.body)
    if (!isValid) {
      return res.status(400).json(errors)
    }

    const { _id } = req.body

    const itemFields = {
      title: req.body.title,
      type: req.body.type,
      variants: req.body.variants
    }

    try {
      if (_id) {
        res.json(
          await Answer.findOneAndUpdate(
            { _id },
            { $set: itemFields },
            { new: true, upsert: true }
          )
        )
      } else {
        const newAnswer = new Answer(itemFields)
        res.json(await newAnswer.save())
      }
    } catch (err) {
      res.status(400).json(err)
    }
  }
)

/**
 * ! @route   POST api/answers/all
 * * @desc    Get All Answers
 * ! @access  Private
 */
router.post(
  '/all',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    // TODO: check user access rights

    try {
      res.json(await Answer.find())
    } catch (err) {
      res.status(400).json(err)
    }
  }
)

/**
 * ! @route   POST api/answers/:id
 * * @desc    Get Answer by id
 * ? @param   id - answer Id
 * ! @access  Private
 */
router.post(
  '/:_id',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    // TODO: check user access rights for answer

    const { _id } = req.params

    try {
      if (_id) {
        res.json(await Answer.findOne({ _id }))
      }
    } catch (err) {
      res.status(400).json(err)
    }
  }
)

module.exports = router
