var express = require('express')
var router = express.Router()
const passport = require('passport')

// Load Models
const Criteria = require('../../models/Criteria')
// Load validation
const validateCriteriaInput = require('../../models/Criteria/validate')

/**
 * ! @route   POST api/criterias
 * * @desc    Save criteria
 * ! @access  Private
 */
router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    // validate survey input
    const { errors, isValid } = validateCriteriaInput(req.body)
    if (!isValid) {
      return res.status(400).json(errors)
    }

    const { _id, title, sid } = req.body

    const itemFields = {
      title,
      sid
    }

    try {
      if (_id) {
        res.json(
          await Criteria.findOneAndUpdate(
            { _id },
            { $set: itemFields },
            { new: true, upsert: true }
          )
        )
      } else {
        if (sid && title) {
          const existedCriteria = await Criteria.findOne({ sid, title })
          if (existedCriteria) {
            res.json(existedCriteria)
          } else {
            const newCriteria = new Criteria(itemFields)
            res.json(await newCriteria.save())
          }
        }
      }
    } catch (err) {
      res.status(400).json(err)
    }
  }
)

/**
 * ! @route   POST api/criterias/all
 * * @desc    Get All Surveys for this research (sid)
 * @param     sid - research Id
 * ! @access  Private
 */
router.post(
  '/all',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    // TODO: check user access rights

    const { sid } = req.body

    try {
      if (sid) {
        res.json(await Criteria.find({ sid }))
      }
    } catch (err) {
      res.status(400).json(err)
    }
  }
)

/**
 * ! @route   POST api/criterias/:id
 * * @desc    Get Criteria by id
 * ? @param   id - criteria Id
 * ! @access  Private
 */
router.post(
  '/:_id',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    // TODO: check user access rights for criteria

    const { _id } = req.params

    try {
      if (_id) {
        res.json(await Criteria.find({ _id }))
      }
    } catch (err) {
      res.status(400).json(err)
    }
  }
)

module.exports = router
