const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const passport = require('passport')
const cors = require('cors')

// app
const app = express()

// public folder
app.use(express.static('public'))

// CORS middleware
// app.use(cors(corsOptions))
app.use(cors())

// Body parser middleware
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

// DB config
const db = require('./config/keys').mongoURI

// Connect to MongoDB
mongoose
  .connect(db, { useNewUrlParser: true })
  .then(() => console.log('MongoDB Connected'))
  .catch(err => console.log(err))

// Passport middleware
app.use(passport.initialize())
// Passport config
require('./models/User')
require('./config/passport')(passport)

// Use Routes
app.use('/api/users', require('./routes/api/users'))
app.use('/api/roles', require('./routes/api/roles'))
app.use('/api/researches', require('./routes/api/researches'))
app.use('/api/surveys', require('./routes/api/surveys'))
app.use('/api/answers', require('./routes/api/answers'))
app.use('/api/questions', require('./routes/api/questions'))
app.use('/api/criterias', require('./routes/api/criterias'))
app.use('/api/respondents', require('./routes/api/respondents'))
app.use('/api/results', require('./routes/api/results'))

const port = process.env.PORT || 5500

app.listen(port, () => console.log(`Server running on port ${port}`))
